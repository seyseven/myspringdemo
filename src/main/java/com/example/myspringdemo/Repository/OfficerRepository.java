package com.example.myspringdemo.Repository;

import com.example.myspringdemo.entity.Officer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository

public interface OfficerRepository extends CrudRepository<Officer, Long> {

    /*
    public List<Officer>findByFullNameLike(String name);
    public List<Officer>findByDateOfBirthGreaterThan(Date date);
    public List<Officer>findByDateReceivedGreaterThan(Date date);
    public List<Officer>findByDateOfDismissalGreaterThan(Date date);
    public List<Officer>findByPositionLike(String name);
    public List<Officer>findByDepartmentLike(String name);
    */

    @Query("select of from Officer of where of.FullName = :FullName")
    Officer findByFullName(@Param("FullName")String FullName);

}
