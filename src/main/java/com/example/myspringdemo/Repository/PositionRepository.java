package com.example.myspringdemo.Repository;

import org.aspectj.weaver.Position;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface PositionRepository extends CrudRepository<Position, Long> {
}
