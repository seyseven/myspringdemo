package com.example.myspringdemo.service;

import com.example.myspringdemo.entity.Officer;

import java.util.List;

public interface OfficerService {
    Officer addOfficer(Officer officer);
    void delete (long id);
    Officer getByFullName(String FullName);
    Officer editOfficer (Officer officer);
    List<Officer> getAll();
}
