package com.example.myspringdemo.entity;

import javax.persistence.*;

@Entity
@Table(name = "DEPARTMENT")

public class Department {

    @Id
    @GeneratedValue
    @Column(name = "Id",nullable = false)
    private Long id;

    @Column(name = "Name_Of_Department",length = 100,nullable = false)
    private String nameOfDepartment;

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id=id;
    }

    public String getName(){
        return nameOfDepartment;
    }
}
