package com.example.myspringdemo.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Officer")

public class Officer extends Basemodel {

    @Id
    @GeneratedValue
    @Column(name="Id",nullable = false)
    private Long id;

    @Column(name="Full_Name")
    private String fullName;

    @Temporal(TemporalType.DATE)
    @Column(name = "Date_Of_Birth")
    private Date dateOfBirth;

    @Temporal(TemporalType.DATE)
    @Column(name="Date_Received")
    private Date dateReceived;

    @Temporal(TemporalType.DATE)
    @Column(name="Date_Of_Desmissal")
    private Date dateOfDismissal;

    @Column(name = "POSITION")
    private String position;

    @Column(name = "DEPARTMENT")
    private String department;



    public Long getId(){
        return id;
    }
    public void setId(Long id){
       this.id=id;
    }

    public String getFullName(){
        return fullName;
    }
    public void setFullName(String fullName) {this.fullName = fullName;}

    public Date getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateReceived(){
        return dateReceived;
    }
    public void setDateReceived(){
        this.dateReceived = dateReceived;
    }

    public Date getDateOfDismissal(){
        return dateOfDismissal;
    }
    public void setDateOfDismissal(){
        this.dateOfDismissal = dateOfDismissal;
    }

    public String getPosition(){
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment(){
        return department;
    }
    public void setDepartment(String department){
        this.department = department;
    }


}
