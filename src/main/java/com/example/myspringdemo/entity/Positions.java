package com.example.myspringdemo.entity;

import javax.persistence.*;

@Entity
@Table(name="POSITIONS")

public class Positions {

    @Id
    @GeneratedValue
    @Column(name="Id",nullable = false)
    private Long id;

    @Column(name="Name_Of_Position",length = 100,nullable = false)
    private  String nameOfPosition;

    @Column(name="Hours_Rate",length = 50,nullable = false)
    private int hoursRate;

}
