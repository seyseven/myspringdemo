package com.example.myspringdemo.entity;

import javax.persistence.*;
import javax.print.attribute.standard.DateTimeAtCompleted;
import javax.print.attribute.standard.DateTimeAtCreation;

@Entity
@Table(name="HISTORY")

public class History {

    @Id
    @GeneratedValue
    @Column(name="Id",nullable = false)
    private Long id;

    @Temporal(TemporalType.TIME)
    @Column(name="Time_In",nullable = false)
    private DateTimeAtCreation timeIn;

    @Temporal(TemporalType.TIME)
    @Column(name="Time_Out", nullable = false)
    private DateTimeAtCompleted timeOut;

    @Column(name = "REASON")
    private String reason;
}
